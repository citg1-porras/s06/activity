/* a.	Books authored by Marjorie Green
The Busy Executive’s Database Guide
You Can Combat Computer Stress!
b.	Books Authored by Michael O’Leary.
Cooking with Computers
c.	Write the author of the Busy Executive Database Guide
Marjorie Green
Abraham Bennet
d.	Identify the Publisher of “But Is it User Friendly?”
Algodata Infosystems
e.	List the books published by Algodata Infosystems
The Busy executive’s Database Guide
Cooking with Computers
Straight Talk About Computers
But Is it User friendly?
Secrets of Silicon Valley
Net Etiquette */


CREATE DATABASE blog_db;
use blog_db;


CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100),
password VARCHAR(300),
datetime_created DATETIME,
PRIMARY KEY(id) 
);


CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
author_id INT NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_users_id
	FOREIGN KEY(author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);


CREATE TABLE posts_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_likes_users_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_posts_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);



CREATE TABLE posts_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_comments_users_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_comments_posts_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);